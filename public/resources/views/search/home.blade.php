@extends('layouts.default')

@section('content')

<div class="container">
  <div class="row">
    <div class="col-sm-12">
        <h1>Search</h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {!! Form::open(['url' => route('searchhome'), 'method' => 'get']) !!}
          <div class="form-group">
            {!! Form::label('keyword', 'Keyword'); !!}
            {!! Form::text('keyword','',['class' => 'form-control']); !!}
          </div>
          <div class="form-group">
            {!! Form::label('Type', 'Type'); !!}
            {!! Form::select('type',$movieKinds,null,['class' => 'form-control']); !!}
          </div>
          <div class="form-group">
            {!! Form::label('From Year', 'From Year'); !!}
            {!! Form::text('startyear','',['class' => 'form-control']); !!}
          </div>
          <div class="form-group">
            {!! Form::label('To Year', 'To Year'); !!}
            {!! Form::text('endyear','',['class' => 'form-control']); !!}
          </div>
          <div class="form-group pull-right">
            {!! Form::submit('Search',['class' => 'btn btn-success']); !!}
          </div>
        {!! Form::close() !!}
    </div>
  </div>
  @if(count($movies) > 0)
    @foreach($movies as $index => $movie)
      @if($index == 0 || $index % 3 == 0)
        @if($index > 0)
          </div>
        @endif
        <div class="row">
      @endif
      @include('partials.moviethumb',['movie' => $movie])
    @endforeach
    @if($movies->count() % 3 != 0)
      </div>
    @endif
    <div class="col-sm-12 col-xs-12 text-align-center">
      {{$movies->appends(request()->query())->links()}}
    </div>
  @endif
</div>

@endsection
