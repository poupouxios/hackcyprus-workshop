@extends('layouts.default')

@section('content')

<div class="container">
  <div class="row">
    <div class="col-sm-12 col-xs-12">
      <h1>{{$movie->title}} ({{$movie->production_year}})</h1>
      @if($movie->movieInfo)
        <p>{{ $movie->movieInfo->info }}</p>
      @else
        <p>No information provided.</p>
      @endif
      <div class="extra-margin-top">
        <h2>Actors</h2>
        @foreach($movie->castInfo as $castInfo)
          <?php
            $gender = isset($castInfo->person->gender)? $castInfo->person->gender : "m";
           ?>
          <div class="col-sm-3 col-xs-12 img-thumbnail text-align-center">
            <img align="center" src="{{ URL::asset('/images/'.$gender.'.png') }}" alt="{{ $castInfo->person->name_pcode_cf }}" />
            <h3>{{$castInfo->person->name}} - (ID: {{$castInfo->person_id}})</h3>
            <h3></h3>
          </div>
        @endforeach
      </div>
    </div>
  </div>
</div>

@endsection
