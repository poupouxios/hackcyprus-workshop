<div class="col-sm-4 col-xs-12 extra-margin-top">
  <div class="col-sm-12 img-thumbnail">
    <a href="{{ route('movieshow', ['id' => $movie->id]) }}" title="{{ $movie->title }}">
      <img class="img-responsive" src="{{ URL::asset('/images/no-thumb.jpg') }}" alt="{{ $movie->title }}" />
    </a>
    <a href="{{ route('movieshow', ['id' => $movie->id]) }}" title="{{ $movie->title }}">
      <h2>{{  $movie->title }} ({{ $movie->production_year}})</h2>
    </a>
    @if($movie->movieInfo)
      <p>{{ \Illuminate\Support\Str::words($movie->movieInfo->info, 20,'....')  }}</p>
    @else
      <p>No information provided.</p>
    @endif
  </div>
</div>
