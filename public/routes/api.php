<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('movies', 'ApiMovieController');
Route::get('movies/search/{title}', 'ApiMovieController@search');

Route::resource('actors', 'ApiActorController');
Route::get('actors/search/{name}', 'ApiActorController@search');
Route::get('actors/moviesthattwoactorsplay/{actor1id}/{actor2id}', 'ApiActorController@moviesthattwoactorsplay');
