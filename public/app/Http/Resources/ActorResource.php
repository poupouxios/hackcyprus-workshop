<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ActorResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'name' => $this->name,
          'imdb_index' => $this->imdb_index,
          'imdb_id' => $this->imdb_id,
          'gender' => $this->gender,
          'name_pcode_cf' => $this->name_pcode_cf,
          'name_pcode_nf' => $this->name_pcode_nf,
          'surname_pcode' => $this->surname_pcode,
          'md5sum' => $this->md5sum,
        ];
    }
}
