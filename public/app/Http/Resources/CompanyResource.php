<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CompanyResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'name' => $this->name,
          'country_code' => $this->country_code,
          'imdb_id' => $this->imdb_id,
          'name_pcode_nf' => $this->name_pcode_nf,
          'name_pcode_sf' => $this->name_pcode_sf,
          'md5sum' => $this->md5sum,
        ];
    }
}
