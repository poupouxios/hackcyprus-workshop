<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CastInfoResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'person' => new ActorResource($this->person),
          'person_role' => new RoleTypeResource($this->personRole),
          'note' => $this->note,
          'nr_order' => $this->nr_order,
          'role_id' => new RoleTypeResource($this->roleType)
        ];
    }
}
