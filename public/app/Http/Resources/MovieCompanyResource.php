<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Models\Company;
use App\Models\CompanyType;

class MovieCompanyResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'company' => new CompanyResource(Company::find($this->company_id)),
          'company_type' => new CompanyTypeResource(CompanyType::find($this->company_type_id)),
          'note' => $this->note
        ];
    }
}
