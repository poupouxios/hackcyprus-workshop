<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class KeywordResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'keyword' => $this->keyword,
          'phonetic_code' => $this->phonetic_code,
        ];
    }
}
