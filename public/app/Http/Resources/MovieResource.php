<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Models\MovieKind as MovieKindModel;

class MovieResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $movieKindData = new MovieKindResource($this->kind);
        $movieCompaniesData = MovieCompanyResource::collection($this->movieCompanies);
        $keywordsData = MovieKeywordResource::collection($this->movieKeywords);
        $actorsData = CastInfoResource::collection($this->castInfo);

        return [
          'id' => $this->id,
          'title' => $this->title,
          'imdb_index' => $this->imdb_index,
          'imdb_id' => $this->imdb_id,
          'production_year' => $this->production_year,
          'phonetic_code' => $this->phonetic_code,
          'episode_of_id' => $this->episode_of_id,
          'season_nr' => $this->season_nr,
          'episode_nr' => $this->episode_nr,
          'series_years' => $this->series_years,
          'md5sum' => $this->md5sum,
          'companies' => $movieCompaniesData,
          'kind' => $movieKindData,
          'keywords' => $keywordsData,
          'actors' => $actorsData,
        ];
    }
}
