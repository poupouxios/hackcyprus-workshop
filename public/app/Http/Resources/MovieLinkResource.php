<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class MovieLinkResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'linked_movies' => new MovieResource($this->linkedMovie),
          'link type' => new LinkTypeResource($this->linkType),
        ];
    }
}
