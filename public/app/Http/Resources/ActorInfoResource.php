<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ActorInfoResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'person_id' => new ActoResourse($this->actor),
          'info_type_id' => new InfoTypeResource($this->infoType),
          'info' => $this->info,
          'note' => $this->note,
        ];
    }
}
