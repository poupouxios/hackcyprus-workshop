<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;
use App\Models\MovieKind;

class SearchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the search form.
     *
     * @return \Illuminate\Http\Response
     */
    public function home(Request $request)
    {
        $validatedData = $request->validate([
            'keyword' => 'max:255',
        ]);

        $movies = [];
        $movieKinds = MovieKind::all()->pluck('kind', 'id')->all();

        $movie = new Movie();
        $movieQuery = $movie->newQuery();

        $keyword = $request->input('keyword', null);
        $type = $request->input('type', 1);

        if ($keyword) {
            $movieQuery->where('title', 'like', '%'.$keyword.'%')
                             ->whereHas('kind', function ($query) use ($type) {
                                 $query->where('id', $type);
                             });
        }

        $startyear = $request->input('startyear', null);
        $endyear = $request->input('endyear', null);

        if ($startyear) {
            $movieQuery->where('production_year', ">=", $startyear);
        }

        if ($endyear) {
            $movieQuery->where('production_year', "<=", $endyear);
        }

        return view('search.home', ['movies' => $movieQuery->paginate(), 'movieKinds' => $movieKinds]);
    }
}
