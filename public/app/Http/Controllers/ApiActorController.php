<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ActorResource;
use App\Models\Actor;
use App\Http\Resources\MovieResource;
use App\Models\Movie;
use App\Models\CastInfo;

class ApiActorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(ActorResource::collection(Actor::paginate()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Search for a specific name.
     *
     * @param  string $name
     * @return \Illuminate\Http\Response
     */
    public function search($name)
    {
        $actors = Actor::where('name', 'like', '%'.$name.'%')->paginate();
        if (count($actors) > 1) {
            return response()->json(ActorResource::collection($actors));
        } elseif (count($actors) == 1) {
            return response()->json(new ActorResource($actors));
        } else {
            return response()->json(["message" => "No results found"]);
        }
    }

    public function moviesthattwoactorsplay($actor1id, $actor2id)
    {
        $moviesOfFirstActor = new CastInfo();
        $movieQuery = $moviesOfFirstActor->newQuery();
        $movieQuery->where('person_id', $actor1id);

        $resultsOfFirstActor = $movieQuery->get()->pluck('movie_id')->toArray();

        $results = Movie::whereHas('castInfo', function ($query) use ($actor2id) {
            $query->where('person_id', $actor2id);
        })
        ->whereIn('id', $resultsOfFirstActor)->get();

        if (count($results) > 0) {
            return response()->json(MovieResource::collection($results));
        } else {
            return response()->json(["message" => "No results found"]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(new ActorResource(Actor::find($id)));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
