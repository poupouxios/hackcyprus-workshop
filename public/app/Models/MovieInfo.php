<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $movie_id
 * @property int $info_type_id
 * @property string $info
 * @property string $note
 */
class MovieInfo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'movie_info';

    /**
     * @var array
     */
    protected $fillable = ['movie_id', 'info_type_id', 'info', 'note'];

    public function movie()
    {
        return $this->belongsTo('App\Models\Movie', 'movie_id');
    }

    public function infoType()
    {
        return $this->belongsTo('App\Models\InfoType');
    }
}
