<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $person_id
 * @property int $movie_id
 * @property int $person_role_id
 * @property string $note
 * @property int $nr_order
 * @property int $role_id
 */
class CastInfo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cast_info';

    /**
     * @var array
     */
    protected $fillable = ['person_id', 'movie_id', 'person_role_id', 'note', 'nr_order', 'role_id'];

    public function person()
    {
        return $this->belongsTo('App\Models\Actor', 'person_id');
    }

    public function movie()
    {
        return $this->belongsTo('App\Models\Movie', 'movie_id');
    }

    public function personRole()
    {
        return $this->belongsTo('App\Models\RoleType', 'person_role_id');
    }

    public function roleType()
    {
        return $this->belongsTo('App\Models\RoleType', 'role_id');
    }
}
