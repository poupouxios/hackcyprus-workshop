<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $country_code
 * @property int $imdb_id
 * @property string $name_pcode_nf
 * @property string $name_pcode_sf
 * @property string $md5sum
 */
class Company extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'company_name';

    /**
     * @var array
     */
    protected $fillable = ['name', 'country_code', 'imdb_id', 'name_pcode_nf', 'name_pcode_sf', 'md5sum'];
}
