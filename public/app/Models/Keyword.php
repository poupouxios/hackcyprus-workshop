<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $keyword
 * @property string $phonetic_code
 */
class Keyword extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'keyword';

    /**
     * @var array
     */
    protected $fillable = ['keyword', 'phonetic_code'];

}
