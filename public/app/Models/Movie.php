<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property string $imdb_index
 * @property int $kind_id
 * @property int $production_year
 * @property int $imdb_id
 * @property string $phonetic_code
 * @property int $episode_of_id
 * @property int $season_nr
 * @property int $episode_nr
 * @property string $series_years
 * @property string $md5sum
 */
class Movie extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'movie';

    /**
     * @var array
     */
    protected $fillable = ['title', 'imdb_index', 'kind_id', 'production_year', 'imdb_id', 'phonetic_code', 'episode_of_id', 'season_nr', 'episode_nr', 'series_years', 'md5sum'];

    public function kind()
    {
        return $this->belongsTo('App\Models\MovieKind', 'kind_id');
    }

    public function movieLink()
    {
        return $this->hasMany('App\Models\MovieLink');
    }

    public function movieCompanies()
    {
        return $this->hasMany('App\Models\MovieCompany');
    }

    public function movieKeywords()
    {
        return $this->hasMany('App\Models\MovieKeyword');
    }

    public function castInfo()
    {
        return $this->hasMany('App\Models\CastInfo');
    }

    public function movieInfo()
    {
        return $this->hasOne('App\Models\MovieInfo');
    }
}
