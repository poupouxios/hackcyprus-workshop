<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $imdb_index
 * @property int $imdb_id
 * @property string $gender
 * @property string $name_pcode_cf
 * @property string $name_pcode_nf
 * @property string $surname_pcode
 * @property string $md5sum
 */
class Actor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'actor';

    /**
     * @var array
     */
    protected $fillable = ['name', 'imdb_index', 'imdb_id', 'gender', 'name_pcode_cf', 'name_pcode_nf', 'surname_pcode', 'md5sum'];

    public function actorInfo()
    {
        return $this->hasOne('App\Models\ActorInfo');
    }
}
