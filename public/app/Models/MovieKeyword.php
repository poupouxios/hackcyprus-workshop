<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $movie_id
 * @property int $keyword_id
 */
class MovieKeyword extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'movie_keyword';

    /**
     * @var array
     */
    protected $fillable = ['movie_id', 'keyword_id'];

    public function movie()
    {
        return $this->belongsTo('App\Models\Movie');
    }

    public function keyword()
    {
        return $this->belongsTo('App\Models\Keyword');
    }
}
