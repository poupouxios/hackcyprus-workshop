<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $info
 */
class InfoType extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'info_type';

    /**
     * @var array
     */
    protected $fillable = ['info'];

}
