<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $person_id
 * @property int $info_type_id
 * @property string $info
 * @property string $note
 */
class ActorInfo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'person_info';

    /**
     * @var array
     */
    protected $fillable = ['person_id', 'info_type_id', 'info', 'note'];

    public function infoType()
    {
        return $this->belongsTo('App\Models\InfoType', 'info_type_id');
    }

    public function actor()
    {
        return $this->belongsTo('App\Models\Actor', 'person_id');
    }
}
