<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $role
 */
class RoleType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'role_type';

    /**
     * @var array
     */
    protected $fillable = ['role'];
}
