<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $kind
 */
class CompanyType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'company_type';

    /**
     * @var array
     */
    protected $fillable = ['kind'];
}
