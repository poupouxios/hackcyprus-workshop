<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $movie_id
 * @property int $company_id
 * @property int $company_type_id
 * @property string $note
 */
class MovieCompany extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'movie_companies';

    /**
     * @var array
     */
    protected $fillable = ['movie_id', 'company_id', 'company_type_id', 'note'];

    public function movie()
    {
        return $this->belongsTo('App\Models\Movie');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function companyType()
    {
        return $this->belongsTo('App\Models\CompanyType');
    }
}
