<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $link
 */
class LinkType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'link_type';

    /**
     * @var array
     */
    protected $fillable = ['link'];
}
