<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $movie_id
 * @property int $linked_movie_id
 * @property int $link_type_id
 */
class MovieLink extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'movie_link';

    /**
     * @var array
     */
    protected $fillable = ['movie_id', 'linked_movie_id', 'link_type_id'];

    public function movie()
    {
        return $this->belongsTo('App\Models\Movie', 'movie_id');
    }

    public function linkedMovie()
    {
        return $this->belongsTo('App\Models\Movie', 'linked_movie_id');
    }

    public function linkType()
    {
        return $this->belongsTo('App\Models\LinkType', 'link_type_id');
    }
}
